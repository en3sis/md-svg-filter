const fs = require('fs-extra')
const { lstatSync, readdirSync } = require('fs')
const moveFile = require('move-file');
const {join} = require('path')

/* RegEx to match svg size */
const regEx = /(.*)24px.svg/gm
const clearnReg = /ic_(.*)_24px/gm
/* Directory name for md repository */
const mdRepo = 'material-design-icons'

/* @TODO: Automaticly remove unusfull folders */
// const uslessFolders = [`${mdRepo}/.git`, `${mdRepo}/iconfont`, `${mdRepo}/sprites`]

/* Filter directories */
const isDirectory = source => lstatSync(source).isDirectory()
const getDirectories = source => readdirSync(source).map(name => join(source, name)).filter(isDirectory)

/* Get all the directories inside md-repo */
const dirArray =  getDirectories(mdRepo)
// console.log('dirArray: ', dirArray);


// moveEverthing ()
readFiles('material-design-icons/action/svg/production')
function readFiles (dir) {
  fs.readdir(dir, function(err, filenames) {
    if (err) {
      console.log(err)
      return;
    }

    matchFiles = filenames.filter(x => x.match(regEx))
    console.log('matchFiles 24px svgs: ', matchFiles.length);

    try {
      for(let i = 0; i < matchFiles.length; i++) {
        const clearIconName = matchFiles[i]

        // var myString = "something format_abc";
        // var myRegexp = /(?:^|\s)format_(.*?)(?:\s|$)/g;
        // var match = myRegexp.exec(myString);
        // console.log(clearIconName[i]); // abc
        console.log(`svg/${matchFiles[i].exec(clearnReg)}`)
        // moveThem(`${dir}/${matchFiles[i]}`, `svg/${matchFiles[i]}`)
      }
    } catch (error) {
      console.log(error)
    }
  })
}

/* Move files function that move one file from X to Y */
function moveThem(cur, dest) {
  (async () => {
    await moveFile(cur, dest)
})()
}

/* Move all files inside all directories from the array */
async function moveEverthing () {
  await dirArray.map(ele => {
    readFiles(`${ele}/svg/production`)
  })
}

