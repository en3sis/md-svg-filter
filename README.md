# Material design SVG filter

Small script to filter every single material icon svg from [material-design-icons](https://github.com/google/material-design-icons)  repository.

### How to:
- Clone this project and install it by runing `npm i`
- Inside main directory clone the material-design-icons repository (make sure it conserve the name)
- Inside md repository delete the following folders: .git, iconfont, sprites
- Once you remove the folders, you can filter all the icons by runing `npm start`

You'll find a new directory called `svgs` that contain every single `24px svg` version of the icons.